
from pendulo_balist import pendulo_balistico
from pendulo_balist import pendulo_balistico_min_vuelta
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')


if __name__=="__main__":
    R,m,M,g=10,5,3,9.8
    angulo = 0
    sol_pend_balistic=pendulo_balistico(R,m,M,g,angulo)
    pend_balistic_vuelta=pendulo_balistico_min_vuelta(R,m,M,g,angulo)

    t = np.arange(0,10,0.01)
    theta_t=sol_pend_balistic.theta()

    #grafica del pendulo balistico luego de la colision
    plt.plot(t,theta_t, label='Oscilador armónico')
    plt.ylabel("theta")
    plt.xlabel("t")
    plt.grid()
    plt.legend()
    plt.title("Osc. Armónico Péndulo Balístico")
    plt.show()
    plt.savefig("osc.armo_pendulo_balistico.png")

    print(f"la velocidad de la bala dado un angulo es: {sol_pend_balistic.vi()}")
    print(f"la velocidad del bloque luego de la colision es: {sol_pend_balistic.vm()}")
    print(f"la velocidad inicial para dar vuelta completa es: {pend_balistic_vuelta.vi()}")


    