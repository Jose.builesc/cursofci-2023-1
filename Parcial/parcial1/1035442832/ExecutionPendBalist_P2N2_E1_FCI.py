
# Importando clases.
from ClassPendBalist_P2N2_E1_FCI import PenduloBalistico, VelMinPendVuelt

if __name__ == '__main__': # Inicializando ejecución.
    # Configurando atributos.
    mb = 0.2 # Masa de la bala.
    mp = 1000#1.5 # Masa del péndulo.
    l = 0.5 # Longitud de la cuerda del péndulo.
    vp = 1.1 # Velocidad del péndulo tras colisión.
    
    # Instanciar/llamar clase.
    sol_pend = PenduloBalistico(mb, mp, l, vp)

    print('El ángulo de desviación del péndulo tras el impacto es: {} °'.format(sol_pend.DesviacionAngulo()))
    print('La velocidad de la bala previo al impacto fue de: {} m/s'.format(sol_pend.VelocidadBala()))
    
    sol_pend.grafPend()

    # Parte del punto de la herencia. Instanciando la clase.
    v_min = VelMinPendVuelt(mb, mp, l, vp)

    print('La velocidad mínima de la bala para que el péndulo dé una vuelta es: {} m/s'.format(v_min.velMinBala()))
    
    v_min.VelocidadBala()