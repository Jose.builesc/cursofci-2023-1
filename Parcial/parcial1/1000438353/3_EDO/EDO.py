# y(x_n+1) = y(x_n) + h*f(x_n,y_n)

import numpy as np
import matplotlib.pyplot as plt
import sympy as sy
import inspect

class SolEDO():

    def __init__(self,a,b,Df,x0,y0,n):
        #print("Inicializando clase EulerMethod")

        self.a=a
        self.b=b
        self.Df=Df
        self.x0=x0
        self.y0=y0
        self.h=(b-a)/n
        self.n=n
        
    #------------------------------ ARRAY X VALORES ENTRE a y b ------------------------------
    def arrayX(self):
        return np.arange(self.a,self.b+self.h,self.h)
    

    #----------------------- ARRAY Y SOLUCIÓN CON MÉTODO DE EULER -------------------------------
    def arrayEuler(self):
        Y=[self.y0]
        for i in range(self.n):
            Y.append(Y[i] + self.h*self.Df(self.arrayX()[i],Y[i]))
        return Y
    
    def FString(self):
        f_lambda=inspect.getsource(self.Df)               # volvemos toda la función lambda un string
        f_string=f_lambda.split(":")[1].strip()           # se elimina lo que está antes del ":"
        f_string=f_string.replace("y","y(x)")             
        f_string=f_string.replace("np.","")
        #print(f_string)
        return f_string
    
    #----------------------- ARRAY Y SOLUCIÓN ANALITICA CON SYMPY -------------------------------
    def arraySympy(self):
        x=sy.Symbol('x')
        y=sy.Function("y")
        f_sympy=sy.sympify(self.FString())                      # se vuelve el string una expresión en sympy
        CI={y(self.x0): self.y0}
        sol = sy.dsolve(y(x).diff(x) - f_sympy, ics=CI)         # solucionamos la ED y'=f(x,y) con condiciones iniciales
        sol_numpy=sy.lambdify(x,sol.rhs)
        #print(sol_numpy)
        return sol_numpy(self.arrayX())

    def Plot(self,nombre):
        font = {'weight' : 'bold', 'size'   : 18}
        plt.rc('font', **font)
        fig = plt.figure(figsize=(10,8))
        plt.title("Solución EDO")
        plt.plot(self.arrayX(),self.arrayEuler(),lw=2, label="Sol. Euler")
        plt.plot(self.arrayX(),self.arraySympy(),lw=2, label="Sol. Sympy")
        plt.legend()
        plt.grid()
        plt.savefig(f"{nombre}")
        print(f"Se ha guardado la imagen como {nombre}")