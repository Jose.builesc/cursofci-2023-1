#DIFERENCIA CON EL COLICIONADOTR LINEA?
#4.5

import matplotlib.pyplot as plt
import numpy as np
import LHC as lh

# Inicializando 
if __name__=='__main__':

    
    R=100       # Radio del colisionador
    r=10        # Radio de las partículas
    N = 25000  # Número de experimentos
    acelerador = lh.colision(R,r,N)
    experimento = acelerador.posicion()
    experimento1 = acelerador.impacto()
    

    experimentos = np.linspace(0,N,len(experimento1))
    plt.figure(figsize=(10,5))
    plt.plot(experimentos, experimento1,color='red',linewidth=2)
    plt.title("Probabilidad de colisión con R = {}, r = {} en un total de {} experimentos".format(R,r,N),fontsize=15)
    plt.xlabel("Experimentos",fontsize=15)
    plt.ylabel("Probabilidad",fontsize=15)
    plt.grid()
    plt.savefig("Probabilidad.png")
    plt.show()


