import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit

class CSVPlotter:
    def __init__(self, csv_file):
        self.csv_file = csv_file

    def fit_quadratic(self):
        # Lee el archivo CSV
        data = pd.read_csv(self.csv_file)

        # Extrae las columnas de datos
        x = data["x"]
        y = data["y"]

        # Define la función cuadrática
        def quadratic_func(x, a, b, c):
            return a * x**2 + b * x + c

        # Realiza el ajuste de la función cuadrática a los datos
        popt, pcov = curve_fit(quadratic_func, x, y)

        # Calcula el error del ajuste (suma de los residuos al cuadrado)
        residuals = y - quadratic_func(x, *popt)
        error = np.sum(residuals**2)

        # Crea un nuevo conjunto de puntos para la función ajustada
        x_fit = np.linspace(x.min(), x.max(), 100)
        y_fit = quadratic_func(x_fit, *popt)

        # Crea la figura y los ejes
        fig, ax = plt.subplots()

        # Grafica la función ajustada
        ax.plot(x_fit, y_fit, 'r-', label="Ajuste cuadrático")

        # Grafica los datos
        ax.plot(x, y, '.', label="Datos")

        # Personaliza los ejes y el título del gráfico
        ax.set_xlabel("Eje X")
        ax.set_ylabel("Eje Y")
        ax.set_title("Ajuste cuadrático de datos")

        # Agrega el error a la leyenda
        legend_text = f"Ajuste cuadrático\nError: {error:.2f}"
        ax.legend([legend_text])

        # Muestra el gráfico
        plt.savefig("ajuste_cuadratico.png")
        plt.show()

        return error

    def fit_gaussian(self):
        # Lee el archivo CSV
        data = pd.read_csv(self.csv_file)

        # Extrae las columnas de datos
        x = data["x"]
        y = data["y"]

        # Define la función gaussiana
        def gaussian_func(x, norm, mean, sigma):
            return norm * np.exp(-(x - mean)**2 / (2 * sigma**2))

        # Realiza el ajuste de la función gaussiana a los datos
        popt, pcov = curve_fit(gaussian_func, x, y)

        # Calcula el error del ajuste (chi-cuadrado)
        residuals = y - gaussian_func(x, *popt)
        chi_squared = np.sum(residuals**2)

        # Crea un nuevo conjunto de puntos para la función ajustada
        x_fit = np.linspace(x.min(), x.max(), 100)
        y_fit = gaussian_func(x_fit, *popt)

        # Crea la figura y los ejes
        fig, ax = plt.subplots()

        # Grafica la función ajustada
        ax.plot(x_fit, y_fit, 'r-', label="Ajuste gaussiano")

        # Grafica los datos
        ax.plot(x, y, '.', label="Datos")

        # Personaliza los ejes y el título del gráfico
        ax.set_xlabel("Eje X")
        ax.set_ylabel("Eje Y")
        ax.set_title("Ajuste gaussiano de datos")

        # Agrega el chi-cuadrado a la leyenda
        legend_text = f"Ajuste gaussiano\nχ²: {chi_squared:.2f}"
        ax.legend([legend_text])

        # Muestra el gráfico
        plt.savefig("ajuste_gaussiano.png")
        plt.show()

        return chi_squared
    
    def fit_general(self):
        # Lee el archivo CSV
        data = pd.read_csv(self.csv_file)

        # Extrae las columnas de datos
        x = data["x"]
        y = data["y"]

        # Define la función general
        def general_func(x, b, norm, mean, sigma):
            return x + b + norm * np.exp(-(x - mean)**2 / (2 * sigma**2))

        # Realiza el ajuste de la función general a los datos
        popt, pcov = curve_fit(general_func, x, y)

        # Calcula el error del ajuste (chi-cuadrado)
        residuals = y - general_func(x, *popt)
        chi_squared = np.sum(residuals**2)

        # Crea un nuevo conjunto de puntos para la función ajustada
        x_fit = np.linspace(x.min(), x.max(), 100)
        y_fit = general_func(x_fit, *popt)

        # Crea la figura y los ejes
        fig, ax = plt.subplots()

        # Grafica la función ajustada
        ax.plot(x_fit, y_fit, 'r-', label="Ajuste general")

        # Grafica los datos
        ax.plot(x, y, '.', label="Datos")

        # Personaliza los ejes y el título del gráfico
        ax.set_xlabel("Eje X")
        ax.set_ylabel("Eje Y")
        ax.set_title("Ajuste general de datos")

        # Agrega el chi-cuadrado a la leyenda
        legend_text = f"Ajuste general\nχ²: {chi_squared:.2f}"
        ax.legend([legend_text])

        # Muestra el gráfico
        plt.savefig("ajuste_general.png")
        plt.show()

        return chi_squared




