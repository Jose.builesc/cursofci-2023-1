import numpy as np
import Tarea1 as tarea

"""
NOTA
4.6

- Revisar los limites de los valores

"""

if __name__ =='__main__':
    
#configuracion
    elec=1.60e-19
    m=9.11e-31
    Ek=18.6*1.6e-19
    x_0=0
    b=0#600e-6
    y_0=0
    theta=np.pi/6
    z_0=0
    t=np.arange(0,100,0.55)
    beta=np.pi/8

#llamar clase
    Tirop=tarea.particulac(elec,m,Ek,b,x_0,y_0,theta,z_0,t,beta)
#llamar metodo
    Tirop.grafica()