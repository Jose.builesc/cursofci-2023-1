import numpy as np
import  matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits import mplot3d # se uso para graficar en 3d
from magnetico import magnetico1

"""
NOTA 
4.5

- Debe manejar errores para valores como m=0
- Buen trabajo!!

"""

if __name__=="__main__":
    masa=9.1*10**(-31) # masa del electron
    energia= 2.98005*10**(-18) # energia pasada a joules
    carga_electron=1.60*10**(-19) # carga electron
    campo= 0.5
    angulo=0.523599 # en radianes.
    frecuenciaang=(carga_electron*campo)/masa
    periodo=2*np.pi/frecuenciaang
    

    magnetico2=magnetico1(energia,angulo,masa,campo,0.8,carga_electron)
    tiempo=np.linspace(0,7*periodo,1000,0.01)
    magnetico2.tiempo=tiempo # VOY ACTUALIZANDO EL TIEMPO.
    magnetico2.grafica() # llamo la grafica.



    
   

    
    
