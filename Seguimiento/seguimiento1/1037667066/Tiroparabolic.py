import numpy as np
import matplotlib.pyplot as plt
import math 


"""
NOTA

5.0
Muy buen trabajo!!

"""
#creación de la clase
class tiro():
    def __init__(self,v0,angulo,y0,x0,gravedad,grafica):

        self.v0=v0 #velocidad inicial
        self.radianangulo=math.radians(angulo) #angulo inicial
        self.y0=y0  #altura inicial
        self.x0=x0  #posicion en x inicial
        self.gravedad=gravedad #gravedad
        self.grafica=grafica
       # self.t=np.linspace(0,self.tv(),100)

    #metodo
    def v0x(self): #velocidad inicial en x
        return self.v0*np.cos(self.radianangulo)
    
    def v0y(self): #velocidad inicial en y
        return self.v0*np.sin(self.radianangulo)
    
    def tv(self):#tiempo de vuelo
        if self.gravedad == 0:
            raise ValueError('Error cambie gravedad')
        else:
            tv=(self.v0y()+np.sqrt(self.v0y()**2+2*self.gravedad*self.y0))/(self.gravedad)
            return tv
    
    def tm(self): #tiempo para el cual v0y=0
        if self.gravedad == 0:
            raise ValueError('Error cambie gravedad')
        else:
            return self.v0y()/self.gravedad
    
    def ym(self): #altura maxima
        return self.y0+self.v0y()*self.tm()-(1/2)*self.gravedad*self.tm()
    def xm(self):
        return self.x0+self.v0x()*self.tv()
    
    def x(self): #posición en x
        t=np.linspace(0,self.tv(),100)
        return self.x0+self.v0x()*t
    
    def y(self): #Posicion en y
        t=np.linspace(0,self.tv(),100)
        return self.y0+self.v0y()*t-1/2*self.gravedad*t**2
    
    def graf(self):#grafica
        plt.figure(figsize=(10,9))
        plt.plot(self.x(), self.y())
        plt.title(self.grafica)
        plt.xlabel('x (m)')
        plt.ylabel('y (m)')
        plt.grid()
        plt.savefig(self.grafica)

class tiro2(tiro): #subclase tiro2
      def __init__(self,v0,angulo,y0,x0,gravedad,grafica,aire):
        super().__init__(v0,angulo,y0,x0,gravedad,grafica)
        self.aire=aire

      def x(self): #modificacion del metodo posicion en x
          t=np.linspace(0,self.tv(),100)
          return self.x0+self.v0x()*t-(1/2)*self.aire*t**2
      
      def xm(self): #modificacion del metodo de la posicion en x maxima
        return self.x0+self.v0x()*self.tv()-(1/2)*self.aire*self.tv()**2

