import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import math 

#creación de la clase
class movimiento():

    #constructor
    def __init__(self,masa,carga,energia,campo,angulo,ciclos):
        self.masa=masa #masa de la particula
        self.carga=carga  #carga de la particula
        self.energia=energia  #Energia cinetica de la particula
        self.campo=campo #campo magnetico en direccion z
        self.radianangulo=math.radians(angulo) #angulo en radianes que forma la velociad de la particula con el campo magnetico
        self.w=(np.abs(self.carga)*self.campo)/self.masa #Frecuencia angular 
        self.n=ciclos #numero de ciclos o vueltas que da la particula 

    #Metodos
    #Phi es un parametro y es igual a 0 para que v0y=0
    def v0(self): #velocidad inicial
        return np.sqrt(2*self.energia/self.masa)
    
    def v0x(self,phi):  #velocidad inicial en x
        return self.v0()*np.sin(self.radianangulo)*np.cos(phi)
    
    def v0y(self,phi):  #velocidad inicial en y
        return self.v0()*np.sin(self.radianangulo)*np.sin(phi)
    
    def v0z(self):    #velocidad inicial en z
        return self.v0()*np.cos(self.radianangulo)
    
    def t(self):     #tiempo del movimiento de la particula
        return np.linspace(0,self.n*self.T(),1000)
    
    def x(self):   #Posicion de la particula en x
        phi=0
        return (1/self.w)*(self.v0x(phi)*np.sin(self.w*self.t())+self.v0y(phi)*(1-np.cos(self.w*self.t())))
    
    def y(self):   #Posicion de la particula en y
        phi=0
        return (1/self.w)*(self.v0y(phi)*np.sin(self.w*self.t())+self.v0x(phi)*(np.cos(self.w*self.t())-1))
    
    def z(self):    #Posicion de la particula en z
         if self.radianangulo ==  np.pi/2:
            return 0
         else:
             return self.v0z()*self.t()
    
    def R(self):  #Radio de la helice
        phi=0
        return self.masa*self.v0x(phi)/(np.abs(self.carga)*self.campo)
    
    def T(self):  #periodo del movimiento
        return 2*np.pi/self.w
    
    def grafica(self): #grafica de la posicion de la particula
        graf=plt.figure(figsize=(10,6))
        ax=graf.add_subplot(111,projection='3d')
        ax.plot(self.x(),self.y(),self.z(),color='green',label='trayectoria')
        #ax.plot(self.x()[0],self.y()[0],self.z()[0],'o',color='r',label='punto inicial')
        ax.set_title('Movimiento Helicoidal del una particula en un campo')
        ax.set_xlabel('x (m)')
        ax.set_ylabel('y (m)')
        ax.set_zlabel('z (m)')
        plt.legend()
        plt.savefig("Movimiento Helicoidal")


