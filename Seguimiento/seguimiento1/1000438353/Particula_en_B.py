import math
import numpy as np
import matplotlib.pyplot as plt


class ParticleInB():

    def __init__(self,q,B0,x0,y0,z0,K0,m,theta,phi):
        
        self.q=q
        self.B0=B0
        self.x0=x0
        self.y0=y0
        self.z0=z0
        self.K0=K0
        self.m=m
        self.radtheta=math.radians(theta)
        self.theta=theta
        self.radphi=math.radians(phi)
        self.w=q*B0/m
    
    print("Inicializando clase ParticleInB")
    
    def Vel0(self):
        vel0=round(np.sqrt(2*self.K0/self.m),3)
        return vel0
    
    def velY(self):
        vel_y=self.Vel0()*round(math.sin(self.radtheta),3)*round(math.sin(self.radphi),3)
        return vel_y
    
    def velZ(self):
        vel_z=self.Vel0()*round(math.cos(self.radtheta),3)
        return vel_z
    
    def velX(self):
        vel_x=self.Vel0()*round(math.sin(self.radtheta),3)*round(math.cos(self.radphi),3)
        return vel_x

    def Time(self):
        time=np.linspace(0,10000,100000)
        return time
    
    def posY(self):
        posy=[self.velX()/self.w*np.cos(self.w*t) + self.velY()/self.w*np.sin(self.w*t) + self.y0  for t in self.Time()]
        return posy
    
    def posZ(self):
        posz=[self.z0 + t*self.velZ() for t in self.Time()]
        return posz
    
    def posX(self):
        posz=[self.velX()/self.w*np.sin(self.w*t) - self.velY()/self.w*np.cos(self.w*t) + self.x0  for t in self.Time()]
        return posz

    def figMp(self):
        plt.figure(figsize=(10,8))
        ax = plt.axes(projection='3d')
        zline = self.posZ()
        xline = self.posX()
        yline = self.posY()
        ax.plot3D(xline, yline, zline,'gray',lw=3)
        ax.set_title('Trayectoria de una carga en un campo magnético, θ={}'.format(self.theta))
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        plt.savefig("Particula_en_B.png")

"""        
        plt.figure(figsize=(10,8))
        plt.plot(self.posX(),self.posZ())
        plt.savefig("Particula_en_B.png")
"""