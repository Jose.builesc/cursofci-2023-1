
import math
from Particula_campomagnetico import particula_cm
import numpy as np
"""
NOTA:

3.4

- Revisar los limites ex: B=0
- Plot no correcto

"""
if __name__=="__main__":

    #llamar atributos

    v0x = 1.50 * 10**5   # velocidad en m/s, note que aunque la velocidad es muy alta es 3 ordenes de magnitud mas baja que la velocidad de la luz
    v0z = 2 * 10**5
    B = 0#0.500            # campo magnetico en tesla
    q = 1.60* 10**(-19)          # carga de un proton en Coulombs
    m = 1.67 * 10**(-27)         # Masa de un proton en kg
    #theta = math.radians(90 - alpha)     # Ángulo entre el campo magnetico y el vector de velocidad.
    
    

    p_cm = particula_cm(v0x,v0z,B,q,m)  # ,theta)
    #print("Tiempo de vuelo:{}".format(t_ra.aTime()))
    p_cm.figMp()

    #print("velocidad en x: {} ".format(p_cm.posX()))
    #print("velocidad en z: {} ".format(p_cm.posY()))
    #print("velocidad en z: {} ".format(p_cm.posZ()))

    #print("frecuencia propia del sitema:{}".format(p_cm.fp()))
    #print("posiciones en y:{}".format(p_cm.posY()))
    
    #print(round(np.sin(4*np.pi)))
    #print(np.sin(2*np.pi))
    #print("array Tiempo:{}".format(p_cm.arrTime()))
