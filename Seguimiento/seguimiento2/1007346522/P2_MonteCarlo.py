import numpy as np
import matplotlib.pyplot as plt
# Buen trabajo
#5.0

class Barrera_MonteCarlo():

    # Unidades naturales: hbar = 1
    global hbar
    hbar = 1

    def __init__(self,V0,m,L = 1):

        if V0 <= 0 or m <= 0 or L <= 0:
            raise ValueError('V0, m y L deben ser positivos')

        self.V0 = V0
        self.m = m
        self.L = L
        self.Energies = []

    # Funcion Transmitancia T(E) o Probabilidad de Transmisión
    def Probabilidad(self,E):

        den = 4*E*(self.V0-E)
        num = self.V0**2*np.sinh(np.sqrt(2*self.m*(self.V0-E))*self.L/hbar)**2

        return 1/(1+num/den)
    
    # Simulación con Monte Carlo
    def Simulacion(self,N_points=100):

        # Creamos un arreglo con valores aleatorios de energía
        # i.e. N partículas cada una con una Enegía entre (0,V0)
        self.Energies = np.random.uniform(0.001,self.V0-0.001,N_points)

        # Creamos un arreglo de valores aleatorios entre (0,1)
        Corte = np.random.uniform(0,1,N_points)

        # Si el valor de corte es menor que la probabilidad de transmisión
        # entonces la partícula pasa la barrera
        T_mask = Corte < self.Probabilidad(self.Energies)

        # Se utiliza un Máscara Booleana buscando optimizar
        return T_mask
    
    # Funcion Grafica
    def Grafica(self,N_points=1000):
        
        # Se simula un número N_points de partículas
        Mask_Sim = self.Simulacion(N_points)
        En_Sim = self.Energies[Mask_Sim]

        # Para graficar la probabilidad de transmisión
        E_ = np.linspace(0.1,self.V0-0.1,1000)
        T_ = self.Probabilidad(E_)

        plt.figure()
        plt.title('Probabilidad de Transmisión vs Energía')
        plt.plot(E_,T_,label = 'Teoría' )

        # Se realiza un histograma de las energías de las partículas que pasaron
        plt.hist(En_Sim,bins = 20,density = True,label = 'Simulación')

        plt.legend()
        plt.grid()
        plt.xlabel('Energía')
        plt.ylabel('Probabilidad de Transmisión')

        plt.savefig('Plot_Barrera_MonteCarlo.png')

