import numpy as np
import sympy as sp

from P1_MonteCarlo import *
from P2_MonteCarlo import *
from Bonus_Ising import *

if __name__=='__main__':
    print(f'Your name is {__name__}')


    ## ----- Punto 1. -----

    ## Definimos la función a integrar
    # def fun(x):
    #     return np.cos(x)*x**2

    ## Definimos los límites de integración
    # Limit_inf = 0.0
    # Limit_sup = np.pi

    # N_puntos = 100000 # Número de Puntos

    # Integral = Integral_MonteCarlo(fun,Limit_inf,Limit_sup,N_puntos)

    # print('Integral exacta:     ',Integral.Integral_sp())
    # print('Integral MonteCarlo: ',Integral.Integral_Area())
    # print('Error:               ',\
    #       round(abs((Integral.Integral_Area()-Integral.Integral_sp())/Integral.Integral_sp())*100,5),'%')

    ## --- Graficar convergencia ---

    # N_eventos_max = 1000
    # Integral.Grafica(N_eventos_max,N_puntos)

    ## Ver gráfica en Plot_Integral_MonteCarlo.png

    ## ---- Punto 2. ----
    ## Efecto Tunel: Barrera de Potencial Unidimensional

    # # Por simplicidad, se toman unidades naturales
    # V0 = 10 # eV
    # m = 0.1 # eV
    # L = 2   # 1/eV

    # Barrera1 = Barrera_MonteCarlo(V0,m,L)

    # # Gráfica de la probabilidad de transmisión
    # Barrera1.Grafica(10000)

    # # Ver gráfica en Plot_Barrera_MonteCarlo.png

    ## Bonus
    # Longitud = 3

    # J = 1.
    # T = 1.5
    # Sig = 2

    # Ising1 = Ising(Longitud,T,J,Sig)

    # # Ising1.Simulacion()

    # Ising1.Graf(T_max = 7,N_max=100,N_points=50)

    ## Ver gráfica en Plot_Cv.png
