import sympy as sp
import numpy as np
import matplotlib.pylab as plt 
#buen trabajo
#5.0
class integracion():
    def __init__(self, x, f, lim1, lim2, n):

        self.x = x
        self.f = f
        self.lim1 = lim1
        self.lim2 = lim2
        self.n = n

    # metodo de integración exacta (vía sympy)
    def int_sim(self):
        return sp.integrate(self.f, (self.x, self.lim1, self.lim2))
    
    # converitmos la función simbólica a númerica con lamdify 
    def f_num(self):
        return sp.lambdify(self.x, self.f, 'numpy')

    # método de integración por monte carlo 
    '''la función f = x**2 * cos(x) tiene un cambio de signo en el intervalo
    [0,pi], luego para utilizar Monte Carlo, consideramos dos rectangulos,
    uno para el área positiva y otro para el área negativa, ambos que encierren
    el área bajo la curva para x entre [lim1, lim2] y y entre un valor h mayor que 
    el máximo de la función.
    '''
    
    def int_mc(self):
        h = 10
        sum_positiva = 0
        sum_negativa = 0
        for i in range(self.n):
            x1 = np.random.uniform(self.lim1, self.lim2)
            y1 = np.random.uniform(-h, h)
            
            # punto dentro del área positiva de la integral
            if (y1 > 0) and (y1 < self.f_num()(x1)):
                sum_positiva += 1

            # punto dentro del área negativa
            if (y1 < 0) and (y1 > self.f_num()(x1)):
                sum_negativa += 1

             # el área de los dos rectangulos es 2*h*(lim2 - lim1)  

        return (sum_positiva - sum_negativa) / self.n * (2 * h * (self.lim2 - self.lim1))
    
    
    # metodo para comparar las soluciones (hay un problema en el for que se queda iterando por siempre)
    # generé la gráfica por fuera de la class 
    def graficar(self):
        l1 = []
        l2 = []
        for i in range(1, 1000):
            self.n = i
            l1.append(i)
            l2.append(self.int_mc())

        plt.figure(figsize=(10, 8))
        plt.plot(l1, l2)
        plt.hlines(-2*np.pi, 0, 1000, color = 'red', label = 'solución real')
        plt.xlabel('iteraciones')
        plt.ylabel('resultado')
        plt.savefig("comparacion.jpg")
        plt.show()
        return 
