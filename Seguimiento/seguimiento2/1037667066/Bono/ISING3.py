import numpy as np
import matplotlib.pyplot as plt
#BUEN TRBAJO, falta ajustar el modelo para tener mayor resolución
# 3.0
#Algoritmo para el modelo de Ising
#Este codigo se trabaja con una matriz de particulas 2x2

L=2 
N=L*L #Tamaño de la matriz de particulas
npaso=1000 #Numero de pasos

np.random.seed(0) #Semilla
#Creamos un diccionario con los vecinos de cada particula 
diccionario_vecinos={i : ((i // L)*L+(i+1)% L, (i+L)%N,(i // L)*L+(i-1)% L, (i-L)%N) for i in range(N)}
print("Diccionario de los vecinos de cada particual:",diccionario_vecinos)

Espines=[np.random.choice([1,-1]) for k in range(N)] #Lista aleatoria de los espines de cada particula
Cv_array = [] #Arreglo de los calores especificos para cada T
T=np.arange(1,7,0.01) #Arreglo de Temperatura en K

for i in T:
    B=1/i #Parametro B=1/T

    Energia=0
    Energia_t=0
    Energia_t_2=0

    for paso in range(npaso):
        k=np.random.randint(0, N-1) #Numeros aleatorios enteros entre 0 a 2
        h=sum(Espines[nn] for nn in diccionario_vecinos[k]) #Sumatoria de los espines vecinos de cada particula
        Espines_old=Espines[k]
        Espines[k]=-1
        if np.random.uniform(0,1)< 1/(1+np.exp(-B*2*h)): #si corte < Probabilidad cambie de espin
            Espines[k] =1

        if Espines[k]!=Espines_old:  
            Energia-=2*Espines[k]*h #Hamiltoniano(energia) de la suma sobre todos los pares de vecinos cercanos

        Energia_t += Energia
        Energia_t_2 += Energia**2

    #Energia_prom=Energia_t/npaso
    #Energia_prom_2=Energia_t_2/npaso

    C_v=B**2/N*((Energia_t_2/npaso)-(Energia_t/npaso)**2)
    Cv_array.append(C_v)

plt.scatter(T,Cv_array,s=3)
plt.ylabel("C_esp")
plt.xlabel("T (K)")
plt.title("Modelo de ISING")
plt.axvline(x=2.269,color="red")
plt.savefig("Modelo de ISING")
plt.show()
