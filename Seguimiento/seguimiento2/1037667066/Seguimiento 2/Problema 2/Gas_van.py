import numpy as np
import matplotlib.pyplot as plt

class Gas_van_der_waals():

    '''
    Clase para determinar el cambio de la energia de libre de Helmhotlz y la energia libre de Gibbs de una mol de gas de Van der Waals
    el cual experimenta una expansión isotermica, a la temperatura T, desde un volumen V_1 hasta un volumen V_2
    '''
    #Diccionario de distintos gases (H_2, N_2, CO_2) con los parametros a [¨Pa m^6 mol^-2] y b [m^3 mol^-1]
    dict_gas={"H_2": [0.01945,0.022e-3], "N_2": [0.13882,0.039e-3], "CO_2": [0.37186,0.043e-3]}
    R=8.3144 #Constante de los gases ideales [J/mol K]

    #Constructor
    def __init__(self,N,Tg,Th,str_gas):
        self.N=N #Numero de iteraciones
        self.Tg=Tg #Temperatura para la energia libre de Gibbs en K
        self.Th=Th #Temperatura para la energia libre de Helmhotlz en K
        self.str_gas=str_gas #String del gas: H_2,N_2,CO_2
        self.v2=np.arange(1,20,0.1) #Arreglo para el volumen 2
        self.v1=self.dict_gas[self.str_gas][1]+0.01 #Valor del volumen 1 V_1=b+0.01

    def fun_1(self,v): #Función 1 a integrar por Montecarlo
        b=self.dict_gas[self.str_gas][1]
        return 1/(v-b)
    
    def fun_2(self,v): #Función 2 a integrar por Montecarlo
        a=self.dict_gas[self.str_gas][0]
        return a/v**2
    
    def fun_3(self,v): #Función 3 a integrar por Montecarlo
        b=self.dict_gas[self.str_gas][1]
        return v/(v-b)**2
    
    #Integral del la función 1,2,3 de V_1 a V_2, para ello se genera un cuadrado que vaya en x de V_1 a V_2 
    # y en y de 0 a el maximo de la función en ese intervalo.

    def integral_1(self):
        In1_area=[] #Lista para guardar las probabilidades
        np.random.seed(2)
        for i in self.v2:
            x1=np.random.uniform(self.v1,i,self.N) #Arreglo de numeros aleatorios de V_1 a V_2
            y1=np.random.uniform(0,self.fun_1(self.v1),self.N) #Arreglo de numeros aleatorios 0 a el maximo de la función en ese intervalo

            interior1=y1<self.fun_1(x1) #Numeros que esten por debajo de la funcion_1
            In1=interior1.sum()/self.N 
            In1_area.append(In1*((i-self.v1)*self.fun_1(self.v1))) #Probabilidad
        return In1_area
    
    def integral_2(self):
        In2_area=[]
        np.random.seed(2)
        for i in self.v2:
            x2=np.random.uniform(self.v1,i,self.N)
            y2=np.random.uniform(0,self.fun_2(self.v1),self.N)

            interior2=y2<self.fun_2(x2)  #Numeros que esten por debajo de la funcion_2
            In2=interior2.sum()/self.N
            In2_area.append(In2*((i-self.v1)*self.fun_2(self.v1)))
        return In2_area
    
    def integral_3(self):
        In3_area=[]
        np.random.seed(2)
        for i in self.v2:
            x3=np.random.uniform(self.v1,i,self.N)
            y3=np.random.uniform(0,self.fun_3(self.v1),self.N)

            interior3=y3<self.fun_3(x3) #Numeros que esten por debajo de la funcion_3
            In3=interior3.sum()/self.N
            In3_area.append(In3*((i-self.v1)*self.fun_3(self.v1)))
        return In3_area
    

    def int_Helmhotz_vanderwalls(self): #Se determina el cambio de la energia libre de Helmhotlz por Montecarlo
        return -self.R*self.Th*np.array(self.integral_1())+np.array(self.integral_2())
    
    def int_Gibbs_vanderwalls(self): #Se determina el cambio de la energia libre de Gibbs por Montecarlo
        return -self.R*self.Tg*np.array(self.integral_3())+2*np.array(self.integral_2())
    
    def integral_exacta_Helmhotlz(self):#Se determina el cambio de la energia libre de Helmhotlz de forma analitica
        yh=[]
        b=self.dict_gas[self.str_gas][1]
        a=self.dict_gas[self.str_gas][0]
        for i in self.v2:
            yh.append(-self.R*self.Th*np.log((i-b)/(self.v1-b))+a*(1/self.v1-1/i))
        return yh
    
    def integral_exacta_Gibbs(self): #Se determina el cambio de la energia libre de Gibbs de forma analitica
        yg=[]
        b=self.dict_gas[self.str_gas][1]
        a=self.dict_gas[self.str_gas][0]
        for i in self.v2:
            yg.append(-self.R*self.Tg*np.log((i-b)/(self.v1-b))+2*a*(1/self.v1-1/i)+self.R*self.Tg*(i/(i-b)-self.v1/(self.v1-b)))
        return yg
    
    def grafica(self): 

        fig, ax=plt.subplots(2,1,figsize=(10,16))
        #Grafica del cambio de la energia libre de Gibbs en función de V_2
        ax[0].plot(self.v2,self.integral_exacta_Gibbs(),color="orange",label="exacta")
        ax[0].plot(self.v2,self.int_Gibbs_vanderwalls(),color="green",label="Montecarlo")
        ax[0].set_xlabel(r"$V_2$")
        ax[0].set_ylabel(r"$\Delta G$")
        ax[0].set_title("Cambio en la energia libre de Gibss a "+str(self.Tg)+"K")
        ax[0].legend()
    
        #Grafica del cambio en la energia libre de Helmhotlz en función de V_2
        ax[1].plot(self.v2,self.integral_exacta_Helmhotlz(),color="red",label="exacta")
        ax[1].plot(self.v2,self.int_Helmhotz_vanderwalls(),color="blue",label="Montecarlo")
        ax[1].set_xlabel(r"$V_2$")
        ax[1].set_ylabel(r"$\Delta A$")
        ax[1].set_title("Cambio en la energia libre de Helmhotz a "+str(self.Th)+"K")
        ax[1].legend()

        fig.suptitle(" Cambio en la energia libre de Helmhotz y Gibbs para "+ self.str_gas,fontsize=20)
        plt.savefig("Energia libre de Helmhotz y Gibbs")

