import serial
import matplotlib.pyplot as plt

ser = serial.Serial('COM9', 9600)  # Cambiar el nombre del puerto serial si es necesario

# Enviar el comando 'tw'
ser.write(b'tw100\n')

# Enviar el comando 'ts'
ser.write(b'ts20000\n')

# Enviar el comando 'start'
ser.write(b'start\n')

x = []
y = []

while True:
    datos = ser.readline().decode().strip()
    if datos == 'finished':  # Si se recibe la palabra 'finished'
        plt.plot(x, y,'.')  # Graficar los datos acumulados
        plt.ylabel('Eje Y')
        plt.xlabel('Eje X')
        plt.show()  # Mostrar la gráfica
        break  # Salir del loop
        
    elementos = datos.split(',')  # Separar los elementos por comas
    for elem in elementos:
        valores = elem.split('*')  # Separar los valores x y y por el asterisco
        if len(valores) == 2:
            try:
                x.append(int(valores[0]))  # Agregar el valor de x a la lista
                y.append(int(valores[1]))  # Agregar el valor de y a la lista
            except ValueError:
                pass  # Si no se puede convertir el valor a entero, ignorar el dato

ser.close()  # Cerrar el puerto serial
